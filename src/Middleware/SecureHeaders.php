<?php
namespace KDA\SecureHeaders\Middleware;
use Closure;

class SecureHeaders
{
    // Enumerate headers which you do not want in your application's responses.
    // Great starting point would be to go check out @Scott_Helme's:
    // https://securityheaders.com/
    //https://report-uri.com/home/generate
    private $unwantedHeaderList = [
        'X-Powered-By',
        'Server',
    ];
    public function handle($request, Closure $next)
    {
        $this->removeUnwantedHeaders(config('kda.sh.remove_headers',$this->unwantedHeaderList));

        $domain_keys=array_keys(config('kda.sh.allowed_domains'));
        $response = $next($request);
        foreach(config('kda.sh.headers',[]) as $header_key => $header){
            foreach($domain_keys as $key ){
                $header = str_replace('{'.$key.'}',implode(' ',config('kda.sh.allowed_domains.'.$key,[])),$header);
            }

            $response->headers->set($header_key, $header);
        }
      
        return $response;
    }
    private function removeUnwantedHeaders($headerList)
    {
        foreach ($headerList as $header)
            header_remove($header);
    }
}
?>