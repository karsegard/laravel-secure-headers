<?php

namespace KDA\SecureHeaders;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{


    use \KDA\Laravel\Traits\HasHelper;

    use \KDA\Laravel\Traits\HasConfig;

    
    protected $configs = [
        'kda/secure_headers.php'  => 'kda.sh'
    ];

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    
}
