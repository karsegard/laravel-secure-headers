<?php

return [
    'allowed_domains'=>[
        'default'=>[
            '*.googleapis.com',
            'unpkg.com',
            '*.unpkg.com',
            '*.jquery.com',
            'localhost',
            '*.gstatic.com',
            '*.jsdelivr.net',
            '*.bootstrapcdn.com',
        ],
        'default-src'=>[
            '*.googleapis.com',
        ],
        'script-src'=>[
            '*.googletagmanager.com',
            '*.google-analytics.com',
            '*.googleapis.com',
            'unpkg.com',
            '*.unpkg.com *.jquery.com localhost *.cloudflare.com *.jsdelivr.net',
            '*.datatables.net',
            '*.jsdelivr.net',
            '*.bootstrapcdn.com',
        ],
        'img-src'=>[
            '*.gstatic.com localhost',
            '*'
        ],
        'font-src'=>[
            '*'
        ],
        'frame-src'=>[
            'localhost'
        ],
        'style-src'=>[
            'localhost',
            '*.jsdelivr.net',
            '*.cloudflare.com',
            '*.gstatic.com',
            '*.jsdelivr.net',
            '*.bootstrapcdn.com',
        ]
    ],
    'remove_headers' => [
        'X-Powered-By',
        'Server',
    ],
    'headers' => [
        'Referrer-Policy'=>'no-referrer-when-downgrade',
        'X-Content-Type-Options'=>'nosniff',
        'X-XSS-Protection'=> '1; mode=block',
        'X-Frame-Options'=> 'SAMEORIGIN',
        'Strict-Transport-Security'=> 'max-age=31536000; includeSubDomains',
        'Content-Security-Policy'=>"default-src 'self' data: 'unsafe-inline' {default-src}; script-src 'unsafe-inline' 'unsafe-eval' 'self' {script-src}; img-src * 'self' data: {img-src}; font-src * 'self' data: {font-src}; frame-src 'self' {frame-src}; style-src 'self'  data: 'unsafe-inline' {style-src}; ",
    ]
];
