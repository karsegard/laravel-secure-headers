## 1 install
    sail artisan vendor:publish --provider="KDA\SecureHeaders\ServiceProvider" --tag="config"

## 2 configure
Add to app/Http/Kernel.php

    protected $middleware = [
        ....
        \KDA\SecureHeaders\Middleware\SecureHeaders::class ,